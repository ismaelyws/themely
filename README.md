# Themely cPanel Plugin

### WordPress Auto-Installer & Theme Directory

#### Themely cPanel plugin is an easy-to-use, one-click WordPress auto-installer combined with a directory of free WordPress themes from talented developers around the world. Not only is it free, it pays you each month!

#### My mission is to help web hosting companies & WordPress theme developers make more money.


## For Web Hosting Companies

- Earn monthly recurring revenue* from the listing and sales of WordPress themes.
- Beginner friendly, simple & fast WordPress auto-installer.
- Reduce WordPress related support tickets (we provide links to documentation & tutorials for WordPress).
- Security minded - we set secure default usernames, passwords and database table prefixes.
- Clear & intuitive cPanel icon & title (so users don't have to search or ask support techs where to install WordPress).
- Offer your clients hundreds of free, beautiful & secure WordPress themes from talented developers around the world.
- We only approve themes to our directory that successfully pass our quality & security review process (we adhere to the official WordPress.org theme security guidelines).
- Only the latest, stable version of WordPress installed (the plugin downloads the latest version directly from the WordPress.org repository and doesn't modify any of the core files).

***Revenue**: A fixed amount (TBD) per active cPanel account.*


## For WordPress Theme Developers

- Market your free or freemium* themes directly to your target audience (bypass Google, WordPress.org, ThemeForest & Mojo Marketplace).
- Guaranteed minimum number of daily downloads (avoid over-saturated & hyper-competitive marketplaces & directories).
- Proportional listing fees, billed monthly (based on a multiple of the amount you sell a theme upgrade).
- Cancel anytime, no contract, no cancellation fees (listing fees will be prorated by day, for example, if listing fees costs $200 a month but you cancel half way trough, we'll only charge you $100).
- Short review process (less than 7 days compared to 10-14 days with ThemeForest.net & 3-5 months with WordPress.org)

*freemium = free with option to upgrade*

## Screenshot

![Themely cPanel Plugin](https://raw.githubusercontent.com/ismaelyws/themely/master/assets/themely-cpanel-screenshot.png)


## Live Demo

I've set up a tiny cloud server with a cPanel license for you to try and test the plugin.

URL: https://li2000-228.members.linode.com/cpanel

Username: `wpthemespot`

Password: `4mIAucrSDx9zNUUFOh`

Once logged in, scroll down to the **Software section** and click **Install WordPress**.

**Important Notice**: The Themely cPanel plugin is currently in public BETA and still under development, you will notice that the directory only contains 12 themes at the moment. I am currently in the process of adding themes to the directory and it will grow in number.


## System Requirements

A Linux server running CentOS 6 or 7 with a valid cPanel/WHM license. For information on cPanel/WHM system requirements visit https://documentation.cpanel.net/display/68Docs/Installation+Guide+-+System+Requirements



## Installation Instructions

To **install** the plugin login to your server as root and run the following commands:

`wget -N http://172.105.14.14/files/cpanel/install.sh`

`chmod 755 install.sh`

`./install.sh`


## Removal Instructions

To **uninstall** the plugin login to your server as root and run the following commands:

`wget -N http://172.105.14.14/files/cpanel/uninstall.sh`

`chmod 755 uninstall.sh`

`./uninstall.sh`


## Get Help/Support

Hi! My name is Hans Desjarlais and I'm the founder & creator of Themely. Whether you're a hosting company or a theme developer, I'm your primary point of contact. If it's to get assistance with my plugin/service or to suggest new features; I'm here to listen.

**Here's how you can reach me:**

Submit support ticket: https://github.com/ismaelyws/themely/issues (click on the green **New Issue** button)

Email: ismaelyws [at] gmail [dot] com

Twitter Direct Message: https://twitter.com/messages/compose?recipient_id=ismaelyws

Phone: +1 (514) 883-0132 (please only use this in case of emergency)

Time Zone: Eastern Standard Time (GMT -4)